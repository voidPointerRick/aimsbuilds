echo off
echo "Resetting Uncommited Changes and Syncing Git State
git reset --hard
echo "Pulling Newest Build Of ASPIRE Application"
git pull
echo "Checking out 'master' branch"
git checkout master
pause
