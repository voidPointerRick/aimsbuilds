1. Open the 'create_db_firebird.sql' and change the first line to the following:
CREATE DATABASE <Path To FDB> page_size 8192 user '<Username>' password '<Password>' DEFAULT CHARACTER SET UTF8;

--OR--

CREATE DATABASE <Server>:<Database Alias> page_size 8192 user '<Username>' password '<Password>' DEFAULT CHARACTER SET UTF8;

Where: 
	  <Server> = The name of the firebird server. ("<Server>:" part is optional if the server is localhost)
	  <Database Alias> = The alias of the database in {firebird_dir}/databases.conf or {firebird_dir}/alias.conf
      <Path To FDB> = The absolute path to the firebird database (*.FDB or *.GDB) file you wnt to create.
      <Username> = Your Firebird User Name (Default is 'SYSDBA')
      <Password> = Password For The Selected User (Default password for SYSDBA is 'masterkey')

2. Save and run the 'create_db_firebird.sql' script in isql to create the empty database structure in firebird.
      isql.exe -q -i create_db_firebird.sql

------------------------------------------
Firebird 2.5:
IMS = C:\ims_firebird\IMS.FDB
------------------------------------------
Firebird 3.0:
IMS = C:/ims_firebird/IMS.FDB
{
	RemoteAccess = true
}
------------------------------------------
	  
3. Enable WireCrypt in firebird.conf and restart the Firebird DefaultInstance
	  Firebird driver used in the application will not work unless encryption is enabled.
      (See: http://firebirdsql.org/file/documentation/release_notes/html/en/3_0/rnfb30-compat-legacyauth.html)

4. Open the DatabaseTransfer application.
	  DatabaseTransfer.exe

5. Set the location of the IMS dBase Database using 'Select DBF Database Location'

6. Click 'Change Firebird Settings' and enter the user settings and the location of the database entered in (1).

7. Check the 'Wipe data before insert' option.

8. Click 'Begin Data Transfer' and wait for it to complete. 
	Progress can be seen using the in-app console output.
	
9. For large tables, data is transferred to the database in clusters of size 250 rows each. If a single statement in a cluster fails, the entire cluster fails.
	Errors, their respective tables and sql insert statements can be seen by clicking the 'Show Errors' button.
