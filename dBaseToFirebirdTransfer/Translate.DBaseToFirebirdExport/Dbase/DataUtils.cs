﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Drawing;

namespace Dbase
{
    /// <summary>
    /// Access dBase tables within the folder specified.
    /// 
    /// SQL Queries cannot restrict (WHERE) unless column name is <=10 characters long.
    /// </summary>
    public class DataUtils
    {
        private string currentFolder = null;

        public bool TestConnection(string folder)
        {
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = ConnectionString(folder);
            try
            {
                conn.Open();
                Thread.Sleep(100);
                if (conn.State == ConnectionState.Open)
                {
                    try
                    {
                        string Sql = "SELECT * FROM categories";
                        OleDbCommand query = new OleDbCommand(Sql, conn);

                        OleDbDataAdapter DA = new OleDbDataAdapter(query);
                        DataTable DTable = new DataTable();
                        DA.Fill(DTable);
                        if (DTable.Columns.Count == 2)
                        {
                            conn.Close();
                            return true;
                        }
                    }
                    catch (ThreadAbortException e)
                    {
                        if (conn != null)
                        {
                            conn.Close();
                            conn.Dispose();
                        }
                        throw e;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message + "\r\n" + e.StackTrace);
                    }
                    conn.Close();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(e.Message + "\r\n" + e.StackTrace, "DB Error");
            }
            return false;
        }

        internal string GetFolder()
        {
            return currentFolder;
        }

        private static DataUtils _instance = null;

        public static DataUtils Instance
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                if (_instance == null) _instance = new DataUtils();
                return _instance;
            }
        }

        private static string GenKey()
        {
            Random r = new Random(Environment.TickCount);
            return r.NextDouble().ToString() + DateTime.Now.ToLongDateString() + r.Next(1000, 500000);
        }

        public string INDEX_DUPLICATES_KEY { get; private set; }

        /// <summary>
        /// Gets a new open connection to the database specified by currentFolder
        /// </summary>
        private OleDbConnection Connection
        {
            get
            {
                try
                {
                    OleDbConnection c = new OleDbConnection();
                    c.ConnectionString = ConnectionString(currentFolder);
                    c.Open();
                    return c;
                }
                catch (ThreadAbortException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + "\r\n" + e.StackTrace);
                    return null;
                }
            }
        }

        private Dictionary<string, DataTable> TableCache;
        private readonly bool DEF_ISOLATION_MODE = true;

        private DataUtils()
        {
            TableCache = new Dictionary<string, DataTable>();
        }

        public bool OpenConnection(string folder)
        {
            if (TestConnection(folder))
            {
                this.currentFolder = folder;
                return true;
            }
            return false;
        }

        private string ConnectionString(string folder)
        {
            return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + folder + ";Extended Properties=dBASE IV;OLE DB Services=-2;";
        }

        public bool IsOpen()
        {
            //if (Conn == null) return false;
            //return Conn.State == ConnectionState.Open;
            if (currentFolder == null) return false;
            if (currentFolder.Contains("\r") || currentFolder.Contains("\n"))
            {
                currentFolder = currentFolder.Replace("\r", "").Replace("\n", "");
            }
            return true;//currentFolderTestConnection(currentFolder);
        }

        public void CloseConnection()
        {
            currentFolder = null;
        }

        public DataTable GetTable(string tableName)
        {
            if (!ExistsTable(tableName))
            {
                //Table not loaded, load now.
                if (!LoadTable(tableName))
                {
                    //Table failed to load. Return.
                    return null;
                }
                //Table is loaded.
            }
            return TableCache[tableName];
        }

        public Dictionary<object, object> Index(string tableName, int uniqueIDColumn, int mappedValueColumn)
        {
            return Index(tableName, uniqueIDColumn, mappedValueColumn, false);
        }

        public Dictionary<object, object> Index(string tableName, int uniqueIDColumn, int mappedValueColumn, bool ignoreDuplicateValuesInUniqueColumn)
        {
            //if (IndexCache.ContainsKey(tableName))
            //    return IndexCache[tableName];
            Dictionary<object, object> index = Index(GetTable(tableName), uniqueIDColumn, mappedValueColumn, ignoreDuplicateValuesInUniqueColumn);
            //IndexCache.Add(tableName, index);
            return index;
        }

        public Dictionary<object, object> Index(DataTable table, int uniqueIDColumn, int mappedValueColumn)
        {
            return Index(table, uniqueIDColumn, mappedValueColumn, false);
        }

        /// <summary>
        /// Creates a dictionary index mapping one unique table column (the key) directly to another column (the value).
        /// </summary>
        /// <param name="tableName">The name of the table</param>
        /// <param name="uniqueIDColumn">The unique key column</param>
        /// <param name="mappedValueColumn">The value column. (Use -1 to map a key to the position of the row in the table, or use -2 to map the key to the DataRow object itself)</param>
        /// <param name="ignoreDuplicateValuesInUniqueColumn">If true, duplicates in the key column are logged and ignored.</param>
        /// <returns>The Index as a Dictionary</returns>
        public Dictionary<object, object> Index(DataTable table, int uniqueIDColumn, int mappedValueColumn, bool ignoreDuplicateValuesInUniqueColumn)
        {
            if (mappedValueColumn < -2)
            {
                Console.WriteLine("Error. Mapped value must be -2, -1 or >=0");
                return null;
            }
            Dictionary<object, object> index = new Dictionary<object, object>();
            if (ignoreDuplicateValuesInUniqueColumn)
            {
                index.Add(INDEX_DUPLICATES_KEY, new List<NameValuePair>());
            }
            for (int rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
            {
                DataRow r = table.Rows[rowIndex];
                object uid = r[uniqueIDColumn];
                object value = (mappedValueColumn == -1) ? rowIndex : (mappedValueColumn == -2) ? r : r[mappedValueColumn];
                if (index.ContainsKey(uid))
                {
                    if (ignoreDuplicateValuesInUniqueColumn)
                    {
                        ((List<NameValuePair>)index[INDEX_DUPLICATES_KEY]).Add(new NameValuePair(uid, value));
                        continue;
                    }
                    throw new InvalidConstraintException("Values in selected column is not unique");
                }
                index.Add(uid, value);
            }
            return index;
        }

        public Dictionary<object, List<object>> IndexOneToMany(string tableName, int uniqueIDColumn, int mappedValueColumn)
        {
            return IndexOneToMany(tableName, uniqueIDColumn, mappedValueColumn, true);
        }

        public Dictionary<object, List<object>> IndexOneToMany(DataTable table, int uniqueIDColumn, int mappedValueColumn)
        {
            return IndexOneToMany(table, uniqueIDColumn, mappedValueColumn, true);
        }

        public Dictionary<object, List<object>> IndexOneToMany(string tableName, int uniqueIDColumn, int mappedValueColumn, bool trimKeyString)
        {
            return IndexOneToMany(GetTable(tableName), uniqueIDColumn, mappedValueColumn, trimKeyString);
        }

        public Dictionary<object, List<object>> IndexOneToMany(DataTable table, int uniqueIDColumn, int mappedValuesColumn, bool trimKeyString)
        {
            if (mappedValuesColumn < -2)
            {
                Console.WriteLine("Error. Mapped values column must be -2(for object reference), -1(for row index) or >=0 (for cell value)");
                return null;
            }
            if (mappedValuesColumn >= table.Columns.Count)
            {
                Console.WriteLine("Error. Mapped value column ");
                return null;
            }
            Dictionary<object, List<object>> index = new Dictionary<object, List<object>>();
            for (int rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
            {
                DataRow r = table.Rows[rowIndex];
                object uid = r[uniqueIDColumn];
                if (Type.GetTypeCode(uid.GetType()) == TypeCode.String && trimKeyString)
                {
                    uid = uid.ToString().Trim();
                }
                List<object> values;
                if (index.ContainsKey(uid))
                {
                    values = index[uid];
                }
                else
                {
                    index.Add(uid, values = new List<object>());
                }
                object value = (mappedValuesColumn == -1) ? rowIndex : (mappedValuesColumn == -2) ? r : r[mappedValuesColumn];
                values.Add(value);
            }
            return index;
        }

        public bool ExistsTableInDatabase(string tableName)
        {
            OleDbConnection Conn = this.Connection;
            OleDbCommand query = null;
            OleDbDataReader reader = null;
            try
            {
                string Sql = "SELECT count(*) FROM " + tableName;
                query = new OleDbCommand(Sql, Conn);

                reader = query.ExecuteReader();
                if (reader.HasRows)
                    while (reader.Read())
                    {
                    }
                reader.Close();
                reader.Dispose();
                query.Dispose();
                Conn.Close();
                Conn.Dispose();
                return true;
            }
            catch (ThreadAbortException e)
            {
                if (Conn != null)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\r\n" + e.StackTrace);
                if (reader != null) { reader.Close(); reader.Dispose(); }
                if (query != null) query.Dispose();
                if (Conn != null)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
                return false;
            }
        }

        public bool ExistsTable(string tableName)
        {
            return TableCache.ContainsKey(tableName);
        }

        public bool LoadTable(string tableName)
        {
            return LoadTable(tableName, false);
        }
        public bool LoadTable(string tableName, bool reloadIfExists)
        {
            return LoadTable(tableName, reloadIfExists, false);
        }
        /// <summary>
        /// Loads a table into cache
        /// </summary>
        /// <param name="tableName">The name of the table to load</param>
        /// <param name="reloadIfExists">If table already exists and this is true, the table is reloaded.</param>
        /// <param name="isolated">Open a temporary new connection to the database. (Multi-Threaded Loading / Access)</param>
        /// <returns>True if the table was loaded or reloaded. False otherwise.</returns>
        public bool LoadTable(string tableName, bool reloadIfExists, bool isolated)
        {
            if (ExistsTable(tableName))
            {
                if (reloadIfExists)
                {
                    UnloadTable(tableName);
                }
                else
                {
                    return true;
                }
            }
            string Sql = "SELECT * FROM " + tableName;
            DataTable DTable = GetTableUsing(Sql, true);
            if (DTable == null) return false;

            TableCache.Add(tableName, DTable);
            return true;
        }

        /// <summary>
        /// Loads a table into cache
        /// </summary>
        /// <param name="sqlQuery">The SQL select statement used to query the table</param>
        /// <param name="isolated">Open a temporary new connection to the database. (Multi-Threaded Loading / Access)</param>
        /// <returns>True if the table was loaded or reloaded. False otherwise.</returns>
        public DataTable GetTableUsing(string sqlQuery, bool isolated)
        {
            if (!sqlQuery.ToUpper().StartsWith("SELECT"))
            {
                return null;
            }
            OleDbConnection Conn = this.Connection;
            try
            {
                Console.WriteLine("Executing Query: " + sqlQuery);
                OleDbCommand query = new OleDbCommand(sqlQuery, Conn);
                OleDbDataAdapter DA = new OleDbDataAdapter(query);
                DataTable DTable = new DataTable();
                DA.Fill(DTable);
                Console.WriteLine("Execution Complete. {0} Results", DTable.Rows.Count);
                DA.Dispose();
                query.Dispose();

                Conn.Close();
                Conn.Dispose();

                return DTable;
            }
            catch (ThreadAbortException e)
            {
                if (Conn != null)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
                throw e;
            }
            catch (Exception e)
            {
                if (Conn != null)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
                Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
                return null;
            }
        }

        public List<object> Unique(string tableName, int column)
        {
            return Unique(tableName, column, DEF_ISOLATION_MODE);
        }
        public List<object> Unique(string tableName, int column, bool isolated)
        {
            OleDbConnection Conn = this.Connection;
            OleDbCommand query = null;
            OleDbDataReader reader = null;
            try
            {
                string Sql = "SELECT * FROM " + tableName + " ORDER BY 1 ASC";
                query = new OleDbCommand(Sql, Conn);

                reader = query.ExecuteReader();
                List<object> values = new List<object>();
                if (reader.HasRows)
                    while (reader.Read())
                    {
                        object v = reader.GetValue(column);
                        if (!values.Contains(v))
                        {
                            values.Add(v);
                        }
                    }
                reader.Close();
                query.Dispose();

                Conn.Close();
                Conn.Dispose();
                return values;
            }
            catch (ThreadAbortException e)
            {
                if (Conn != null)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
                throw e;
            }
            catch (Exception e)
            {
                if (reader != null) reader.Close();
                if (query != null) query.Dispose();
                if (Conn != null)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
                Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
            }
            return null;
        }

        /// <summary>
        /// Removes a table from cache.
        /// </summary>
        /// <param name="tableName">The name of the table to unload</param>
        /// <returns></returns>
        public bool UnloadTable(string tableName)
        {
            if (ExistsTable(tableName))
            {
                bool rem = TableCache.Remove(tableName);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                return rem;
            }
            return false;
        }

        public void UnloadAll()
        {
            TableCache.Clear();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }

    public class NameValuePair
    {
        public NameValuePair(object name, object value)
        {
            this.name = name;
            this.value = value;
        }

        public object name { get; private set; }
        public object value { get; private set; }
    }

}
