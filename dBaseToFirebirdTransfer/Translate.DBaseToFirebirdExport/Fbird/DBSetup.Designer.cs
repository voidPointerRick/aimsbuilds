﻿namespace Fbird
{
    partial class DBSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Browse = new System.Windows.Forms.Button();
            this.Button_DefPort = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.Input_Database = new System.Windows.Forms.TextBox();
            this.Check_Alias = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Input_Password = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Input_Username = new System.Windows.Forms.TextBox();
            this.Check_Localhost = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Input_Port = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Input_Server = new System.Windows.Forms.TextBox();
            this.Button_TestConnection = new System.Windows.Forms.Button();
            this.Button_OK = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.Label_Text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Button_Browse
            // 
            this.Button_Browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Browse.Location = new System.Drawing.Point(391, 119);
            this.Button_Browse.Name = "Button_Browse";
            this.Button_Browse.Size = new System.Drawing.Size(77, 20);
            this.Button_Browse.TabIndex = 30;
            this.Button_Browse.Text = "Browse...";
            this.Button_Browse.UseVisualStyleBackColor = true;
            this.Button_Browse.Click += new System.EventHandler(this.Button_Browse_Click);
            // 
            // Button_DefPort
            // 
            this.Button_DefPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_DefPort.Location = new System.Drawing.Point(272, 34);
            this.Button_DefPort.Name = "Button_DefPort";
            this.Button_DefPort.Size = new System.Drawing.Size(113, 20);
            this.Button_DefPort.TabIndex = 29;
            this.Button_DefPort.Text = "Use Default Port";
            this.Button_DefPort.UseVisualStyleBackColor = true;
            this.Button_DefPort.Click += new System.EventHandler(this.Button_DefPort_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(72, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Database:";
            // 
            // Input_Database
            // 
            this.Input_Database.Location = new System.Drawing.Point(143, 119);
            this.Input_Database.Name = "Input_Database";
            this.Input_Database.Size = new System.Drawing.Size(242, 20);
            this.Input_Database.TabIndex = 27;
            this.Input_Database.TextChanged += new System.EventHandler(this.Input_TextChanged);
            // 
            // Check_Alias
            // 
            this.Check_Alias.AutoSize = true;
            this.Check_Alias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_Alias.Location = new System.Drawing.Point(143, 144);
            this.Check_Alias.Name = "Check_Alias";
            this.Check_Alias.Size = new System.Drawing.Size(54, 17);
            this.Check_Alias.TabIndex = 26;
            this.Check_Alias.Text = "Alias?";
            this.Check_Alias.UseVisualStyleBackColor = true;
            this.Check_Alias.CheckedChanged += new System.EventHandler(this.Check_Alias_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(72, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Password:";
            // 
            // Input_Password
            // 
            this.Input_Password.Location = new System.Drawing.Point(143, 87);
            this.Input_Password.Name = "Input_Password";
            this.Input_Password.Size = new System.Drawing.Size(242, 20);
            this.Input_Password.TabIndex = 24;
            this.Input_Password.UseSystemPasswordChar = true;
            this.Input_Password.TextChanged += new System.EventHandler(this.Input_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Firebird Username:";
            // 
            // Input_Username
            // 
            this.Input_Username.Location = new System.Drawing.Point(143, 65);
            this.Input_Username.Name = "Input_Username";
            this.Input_Username.Size = new System.Drawing.Size(242, 20);
            this.Input_Username.TabIndex = 22;
            this.Input_Username.TextChanged += new System.EventHandler(this.Input_TextChanged);
            // 
            // Check_Localhost
            // 
            this.Check_Localhost.AutoSize = true;
            this.Check_Localhost.Location = new System.Drawing.Point(391, 14);
            this.Check_Localhost.Name = "Check_Localhost";
            this.Check_Localhost.Size = new System.Drawing.Size(90, 17);
            this.Check_Localhost.TabIndex = 21;
            this.Check_Localhost.Text = "Use localhost";
            this.Check_Localhost.UseVisualStyleBackColor = true;
            this.Check_Localhost.CheckedChanged += new System.EventHandler(this.Check_Localhost_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(103, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Port:";
            // 
            // Input_Port
            // 
            this.Input_Port.Location = new System.Drawing.Point(143, 34);
            this.Input_Port.Name = "Input_Port";
            this.Input_Port.Size = new System.Drawing.Size(126, 20);
            this.Input_Port.TabIndex = 19;
            this.Input_Port.TextChanged += new System.EventHandler(this.Input_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "IMS Firebird Server:";
            // 
            // Input_Server
            // 
            this.Input_Server.Location = new System.Drawing.Point(143, 12);
            this.Input_Server.Name = "Input_Server";
            this.Input_Server.Size = new System.Drawing.Size(242, 20);
            this.Input_Server.TabIndex = 16;
            this.Input_Server.TextChanged += new System.EventHandler(this.Input_TextChanged);
            // 
            // Button_TestConnection
            // 
            this.Button_TestConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_TestConnection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_TestConnection.Location = new System.Drawing.Point(280, 180);
            this.Button_TestConnection.Name = "Button_TestConnection";
            this.Button_TestConnection.Size = new System.Drawing.Size(188, 29);
            this.Button_TestConnection.TabIndex = 18;
            this.Button_TestConnection.Text = "Test Connection";
            this.Button_TestConnection.UseVisualStyleBackColor = true;
            this.Button_TestConnection.Click += new System.EventHandler(this.Button_TestConnection_Click);
            // 
            // Button_OK
            // 
            this.Button_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_OK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_OK.Location = new System.Drawing.Point(280, 219);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(93, 29);
            this.Button_OK.TabIndex = 31;
            this.Button_OK.Text = "OK";
            this.Button_OK.UseVisualStyleBackColor = true;
            this.Button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_Cancel.Location = new System.Drawing.Point(379, 219);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(89, 29);
            this.Button_Cancel.TabIndex = 32;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // Label_Text
            // 
            this.Label_Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Text.Location = new System.Drawing.Point(12, 186);
            this.Label_Text.Name = "Label_Text";
            this.Label_Text.Size = new System.Drawing.Size(257, 66);
            this.Label_Text.TabIndex = 33;
            // 
            // DBSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 261);
            this.Controls.Add(this.Label_Text);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Button_Browse);
            this.Controls.Add(this.Button_DefPort);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Input_Database);
            this.Controls.Add(this.Check_Alias);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Input_Password);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Input_Username);
            this.Controls.Add(this.Check_Localhost);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Input_Port);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Input_Server);
            this.Controls.Add(this.Button_TestConnection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBSetup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Firebird Database Settings";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Button_Browse;
        private System.Windows.Forms.Button Button_DefPort;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Input_Database;
        private System.Windows.Forms.CheckBox Check_Alias;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Input_Password;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Input_Username;
        private System.Windows.Forms.CheckBox Check_Localhost;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Input_Port;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Input_Server;
        private System.Windows.Forms.Button Button_TestConnection;
        private System.Windows.Forms.Button Button_OK;
        private System.Windows.Forms.Button Button_Cancel;
        private System.Windows.Forms.Label Label_Text;
    }
}