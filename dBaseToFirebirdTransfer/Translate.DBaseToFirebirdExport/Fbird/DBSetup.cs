﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fbird
{
    public partial class DBSetup : Form
    {
        private bool tested;
        private IMSSettings settings;

        public DBSetup(IMSSettings settings)
        {
            InitializeComponent();
            this.settings = settings;
            InitSettings();
        }

        private void InitSettings()
        {
            Input_Server.Text = settings.Server == null ? "localhost" : settings.Server;
            Input_Port.Text = (settings.Port < 1024 ? 3050 : settings.Port).ToString();
            Input_Username.Text = settings.User == null ? "SYSDBA" : settings.User;
            Input_Password.Text = settings.Pass == null ? "" : settings.Pass;
            if (settings.Alias == null)//If No Alias, Set Blank Alias
                settings.Set(settings.Server, settings.Port, settings.User, settings.Pass, "---", settings.Location);
            Input_Database.Text = settings.Alias.Equals("---") ? settings.Location : settings.Alias;
            Check_Alias.Checked = !settings.Alias.Equals("---");
            Check_Localhost.Checked = settings.Server == null || settings.Server.ToLower().Equals("localhost");
            tested = false;
        }

        private void Button_TestConnection_Click(object sender, EventArgs e)
        {
            string server = Input_Server.Text;
            string ptxt = Input_Port.Text;
            int port = ptxt.Length == 0 ? 3050 : Convert.ToInt32(ptxt);
            if (port > 65535) port = 3050;
            string user = Input_Username.Text;
            string pass = Input_Password.Text;
            string db = Input_Database.Text;
            bool alias = Check_Alias.Checked;

            List<string> param = new List<string>();
            param.AddRange(new string[] { user, pass, server, port.ToString() });
            if (alias) param.Add(db);
            bool connected = DataUtils.Instance.TestConnection(alias ? null : db, param.ToArray());
            MessageBox.Show(this, connected ? "Connection Success"
                : "Connection Failure", "Test Result");
            tested = connected;
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (!tested)
            {
                MessageBox.Show(this,
                    "Please test the connection and ensure that it works before saving."
                    , "Untested DB Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string ptxt = Input_Port.Text;
            int port = ptxt.Length == 0 ? 3050 : Convert.ToInt32(ptxt);
            if (port > 65535) port = 3050;
            settings.Set(
                Input_Server.Text, port,
                Input_Username.Text, Input_Password.Text,
                Check_Alias.Checked ? Input_Database.Text : null,
                Input_Database.Text
            );

            DialogResult = DialogResult.OK;
            Close();
        }

        private void Button_Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog();
            fbd.Filter = "Firebird Database (*.fdb;*.gdb) | *.fdb;*.gdb";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                Input_Database.Text = fbd.FileName;
                tested = false;
            }
        }

        private void Button_DefPort_Click(object sender, EventArgs e)
        {
            Input_Port.Text = "3050";
        }

        private void Check_Localhost_CheckedChanged(object sender, EventArgs e)
        {
            Input_Server.ReadOnly = Check_Localhost.Checked;
            if (Check_Localhost.Checked)
            {
                Input_Server.Text = "localhost";
                if (!Check_Alias.Checked) Button_Browse.Enabled = true;
            }
        }

        private void Check_Alias_CheckedChanged(object sender, EventArgs e)
        {
            Button_Browse.Enabled = (!Check_Alias.Checked && Check_Localhost.Checked);
        }

        public void Msg(string text)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(Msg), text);
                return;
            }
            this.Label_Text.Text = text;
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void Input_TextChanged(object sender, EventArgs e)
        {
            tested = false;
        }
    }
}
