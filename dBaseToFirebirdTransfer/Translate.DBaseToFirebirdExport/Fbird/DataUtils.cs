﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;
using System.Drawing;

namespace Fbird
{

    /// <summary>
    /// DataUtils reflection for Firebird
    /// </summary>
    public class DataUtils
    {
        private string currentFolder = null;
        public string ConnectionString { get; private set; }
        private string[] parameters = null;
        //private static readonly string IMS_DATABASE = "IMS.FDB";

        public bool TestConnection(string database_name, params string[] parameters)
        {
            InitConnectionString(database_name, parameters);
            try
            {
                using (FbConnection conn = new FbConnection(ConnectionString))
                {
                    conn.Open();
                    try
                    {
                        string Sql = "SELECT * FROM categories";
                        using (FbCommand query = new FbCommand(Sql, conn))
                        using (FbDataAdapter DA = new FbDataAdapter(query))
                        {

                            DataTable DTable = new DataTable();
                            DA.Fill(DTable);
                            if (DTable.Columns.Count == 2)
                            {
                                return true;
                            }
                        }
                    }
                    catch (ThreadAbortException e)
                    {
                        throw e;
                    }
                    catch (Exception e)
                    {
                        e.PrintStackTrace();
                    }
                }
            }
            catch (Exception e)
            {
                e.PrintStackTrace();

                //MessageBox.Show(e.Message + "\r\n" + e.StackTrace, "DB Error");
            }
            return false;
        }

        internal string GetFolder()
        {
            return currentFolder;
        }

        private static DataUtils _instance = null;

        public static DataUtils Instance
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                if (_instance == null) _instance = new DataUtils();
                return _instance;
            }
        }

        private static string GenKey()
        {
            Random r = new Random(Environment.TickCount);
            return r.NextDouble().ToString() + DateTime.Now.ToLongDateString() + r.Next(1000, 500000);
        }

        public string INDEX_DUPLICATES_KEY { get; private set; }

        private Dictionary<string, DataTable> TableCache;
        private readonly bool DEF_ISOLATION_MODE = true;

        private DataUtils()
        {
            if (INDEX_DUPLICATES_KEY == null)
                INDEX_DUPLICATES_KEY = GenKey();
            TableCache = new Dictionary<string, DataTable>();
            //IndexCache = new Dictionary<string, Dictionary<object, object>>();
        }

        public bool OpenConnection(string folder, params string[] parameters)
        {
            if (TestConnection(folder, parameters))
            {
                this.currentFolder = folder;
                this.parameters = (parameters == null || parameters.Length == 0)
                    ? null : parameters;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database">The folder or database alias containing the IMS database.</param>
        /// <param name="parameters">[Username, Password[, Server Address, Port[, Alias ] ] ] (In that order)</param>
        /// <returns></returns>
        private void InitConnectionString(string database, params string[] parameters)
        {
            string user = "SYSDBA";
            string pass = "1ruqc7";
            string server = "localhost";
            string port = "3050";
            string alias = null;

            if (parameters == null || parameters.Length == 0)
            {
                if (this.parameters != null)
                    parameters = this.parameters;
            }
            if (parameters.Length > 1)
            {
                user = parameters[0];
                pass = parameters[1];
            }
            if (parameters.Length > 3)
            {
                server = parameters[2];
                port = parameters[3];
            }
            if (database == null && parameters.Length > 4)
            {
                alias = parameters[4];
            }

            if (alias != null)
            {
                database = alias;
            }
            string connectionString =
                    "User=" + user + ";" +
                    "Password=" + pass + ";" +
                    "Database=" + database + ";" +
                    "DataSource=" + server + ";" +
                    "Port=" + port + ";" +
                    "Dialect=3;" +
                    "Charset=UTF8;" +
                    "Role=;" +
                    "Connection lifetime=30;" +
                    "Pooling=true;" +
                    "MinPoolSize=0;" +
                    "MaxPoolSize=100;" +
                    "Packet Size=8192;" +
                    "ServerType=0";
            this.ConnectionString = connectionString;
        }

        private void InitPool()
        {
            //Initialize 10 Connection in the pool.
            for (int i = 0; i < 10; i++)
            {
                using (FbConnection c = new FbConnection(ConnectionString))
                {
                    c.Open();
                }
            }
        }

        public bool IsOpen()
        {
            if (currentFolder == null) return false;
            return true;
        }

        public void CloseConnection()
        {
            currentFolder = null;
        }

        public DataTable GetTable(string tableName)
        {
            if (!ExistsTable(tableName))
            {
                //Table not loaded, load now.
                if (!LoadTable(tableName))
                {
                    //Table failed to load. Return.
                    return null;
                }
                //Table is loaded.
            }
            return TableCache[tableName];
        }

        #region Soft-Indexing Functions

        public Dictionary<object, object> Index(string tableName, int uniqueIDColumn, int mappedValueColumn)
        {
            return Index(tableName, uniqueIDColumn, mappedValueColumn, false);
        }

        public Dictionary<object, object> Index(string tableName, int uniqueIDColumn, int mappedValueColumn, bool ignoreDuplicateValuesInUniqueColumn)
        {
            //if (IndexCache.ContainsKey(tableName))
            //    return IndexCache[tableName];
            Dictionary<object, object> index = Index(GetTable(tableName), uniqueIDColumn, mappedValueColumn, ignoreDuplicateValuesInUniqueColumn);
            //IndexCache.Add(tableName, index);
            return index;
        }

        public Dictionary<object, object> Index(DataTable table, int uniqueIDColumn, int mappedValueColumn)
        {
            return Index(table, uniqueIDColumn, mappedValueColumn, false);
        }

        /// <summary>
        /// Creates a dictionary index mapping one unique table column (the key) directly to another column (the value).
        /// </summary>
        /// <param name="tableName">The name of the table</param>
        /// <param name="uniqueIDColumn">The unique key column</param>
        /// <param name="mappedValueColumn">The value column. (Use -1 to map a key to the position of the row in the table, or use -2 to map the key to the DataRow object itself)</param>
        /// <param name="ignoreDuplicateValuesInUniqueColumn">If true, duplicates in the key column are logged and ignored.</param>
        /// <returns>The Index as a Dictionary</returns>
        public Dictionary<object, object> Index(DataTable table, int uniqueIDColumn, int mappedValueColumn, bool ignoreDuplicateValuesInUniqueColumn)
        {
            if (mappedValueColumn < -2)
            {
                Console.WriteLine("Error. Mapped value must be -2, -1 or >=0");
                return null;
            }
            Dictionary<object, object> index = new Dictionary<object, object>();
            if (table == null) return index;
            if (ignoreDuplicateValuesInUniqueColumn)
            {
                index.Add(INDEX_DUPLICATES_KEY, new List<NameValuePair>());
            }
            for (int rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
            {
                DataRow r = table.Rows[rowIndex];
                object uid = r[uniqueIDColumn];
                object value = (mappedValueColumn == -1) ? rowIndex : (mappedValueColumn == -2) ? r : r[mappedValueColumn];
                if (index.ContainsKey(uid))
                {
                    if (ignoreDuplicateValuesInUniqueColumn)
                    {
                        ((List<NameValuePair>)index[INDEX_DUPLICATES_KEY]).Add(new NameValuePair(uid, value));
                        continue;
                    }
                    throw new InvalidConstraintException("Values in selected column is not unique");
                }
                index.Add(uid, value);
            }
            return index;
        }

        public Dictionary<object, List<object>> IndexOneToMany(string tableName, int uniqueIDColumn, int mappedValueColumn)
        {
            return IndexOneToMany(tableName, uniqueIDColumn, mappedValueColumn, true);
        }

        public Dictionary<object, List<object>> IndexOneToMany(DataTable table, int uniqueIDColumn, int mappedValueColumn)
        {
            return IndexOneToMany(table, uniqueIDColumn, mappedValueColumn, true);
        }

        public Dictionary<object, List<object>> IndexOneToMany(string tableName, int uniqueIDColumn, int mappedValueColumn, bool trimKeyString)
        {
            return IndexOneToMany(GetTable(tableName), uniqueIDColumn, mappedValueColumn, trimKeyString);
        }

        public Dictionary<object, List<object>> IndexOneToMany(DataTable table, int uniqueIDColumn, int mappedValuesColumn, bool trimKeyString)
        {
            if (mappedValuesColumn < -2)
            {
                Console.WriteLine("Error. Mapped values column must be -2(for object reference), -1(for row index) or >=0 (for cell value)");
                return null;
            }
            if (mappedValuesColumn >= table.Columns.Count)
            {
                Console.WriteLine("Error. Mapped value column ");
                return null;
            }
            Dictionary<object, List<object>> index = new Dictionary<object, List<object>>();
            for (int rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
            {
                DataRow r = table.Rows[rowIndex];
                object uid = r[uniqueIDColumn];
                if (Type.GetTypeCode(uid.GetType()) == TypeCode.String && trimKeyString)
                {
                    uid = uid.ToString().Trim();
                }
                List<object> values;
                if (index.ContainsKey(uid))
                {
                    values = index[uid];
                }
                else
                {
                    index.Add(uid, values = new List<object>());
                }
                object value = (mappedValuesColumn == -1) ? rowIndex : (mappedValuesColumn == -2) ? r : r[mappedValuesColumn];
                values.Add(value);
            }
            return index;
        }

        #endregion

        public bool ExistsTableInDatabase(string tableName)
        {
            try
            {
                string Sql = "select rdb$relation_name from rdb$relations "
                            + "where rdb$view_blr is null "
                            + "and (rdb$system_flag is null or rdb$system_flag=0) "
                            + "and LOWER(rdb$relation_name)='" + tableName.ToLower() + "';";
                Console.WriteLine("SQL = " + Sql);
                DataTable t = GetTableUsing(Sql);
                if (t != null && t.Rows.Count == 1 && t.Columns.Count == 1)
                {
                    if (!t.Rows[0][0].Equals(DBNull.Value))
                    {
                        return true;
                    }
                }
            }
            catch (Exception) { }
            return false;
        }

        public bool ExistsTable(string tableName)
        {
            return TableCache.ContainsKey(tableName);
        }

        public bool LoadTable(string tableName)
        {
            return LoadTable(tableName, false);
        }
        public bool LoadTable(string tableName, bool reloadIfExists)
        {
            return LoadTable(tableName, reloadIfExists, false);
        }
        /// <summary>
        /// Loads a table into cache
        /// </summary>
        /// <param name="tableName">The name of the table to load</param>
        /// <param name="reloadIfExists">If table already exists and this is true, the table is reloaded.</param>
        /// <param name="isolated">Open a temporary new connection to the database. (Multi-Threaded Loading / Access)</param>
        /// <returns>True if the table was loaded or reloaded. False otherwise.</returns>
        public bool LoadTable(string tableName, bool reloadIfExists, bool isolated)
        {
            if (ExistsTable(tableName))
            {
                if (reloadIfExists)
                {
                    UnloadTable(tableName);
                }
                else
                {
                    return true;
                }
            }
            string Sql = "SELECT * FROM " + tableName;
            DataTable DTable = GetTableUsing(Sql);
            if (DTable == null) return false;

            TableCache.Add(tableName, DTable);
            return true;
        }

        public DataTable GetTableUsing(string sqlQuery)
        {
            return GetTableUsing(sqlQuery, DEF_ISOLATION_MODE);
        }

        /// <summary>
        /// Loads a table into cache
        /// </summary>
        /// <param name="sqlQuery">The SQL select statement used to query the table</param>
        /// <param name="isolated">Open a temporary new connection to the database. (Multi-Threaded Loading / Access)</param>
        /// <returns>True if the table was loaded or reloaded. False otherwise.</returns>
        public DataTable GetTableUsing(string sqlQuery, bool isolated)
        {
            if (!sqlQuery.ToUpper().StartsWith("SELECT"))
            {
                throw new ArgumentException("Expected an SQL SELECT Query");
            }
            using (FbConnection Conn = new FbConnection(ConnectionString))
            {
                try
                {
                    Conn.Open();
                    Console.WriteLine("Executing Query: " + sqlQuery);
                    using (FbCommand query = new FbCommand(sqlQuery, Conn))
                    using (FbDataAdapter DA = new FbDataAdapter(query))
                    {
                        DataTable DTable = new DataTable();
                        DA.Fill(DTable);

                        return DTable;
                    }
                }
                catch (ThreadAbortException e)
                {
                    if (Conn != null)
                    {
                        Conn.Close();

                    }
                    throw e;
                }
                catch (Exception e)
                {
                    if (Conn != null)
                    {
                        Conn.Close();

                    }
                    Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
                    return null;
                }
            }
        }


        public List<object> Unique(string tableName, int column)
        {
            return Unique(tableName, column, DEF_ISOLATION_MODE);
        }
        public List<object> Unique(string tableName, int column, bool isolated)
        {
            using (FbConnection Conn = new FbConnection(ConnectionString))
            {
                try
                {
                    Conn.Open();
                    string Sql = "SELECT * FROM " + tableName + " ORDER BY 1 ASC";
                    using (FbCommand query = new FbCommand(Sql, Conn))
                    using (FbDataReader reader = query.ExecuteReader())
                    {
                        List<object> values = new List<object>();
                        if (reader.HasRows)
                            while (reader.Read())
                            {
                                object v = reader.GetValue(column);
                                if (!values.Contains(v))
                                {
                                    values.Add(v);
                                }
                            }
                        return values;
                    }
                }
                catch (ThreadAbortException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
                }
                return null;
            }
        }


        public object[] Find(string tableName, int column, object value)
        {
            return Find(tableName, column, value, DEF_ISOLATION_MODE);
        }
        public object[] Find(string tableName, int column, object value, bool isolated)
        {
            using (FbConnection Conn = new FbConnection(ConnectionString))
            {
                try
                {
                    Conn.Open();
                    string Sql = "SELECT * FROM " + tableName + " ORDER BY 1 ASC";
                    using (FbCommand query = new FbCommand(Sql, Conn))
                    using (FbDataReader reader = query.ExecuteReader())
                    {
                        if (reader.HasRows)
                            while (reader.Read())
                            {
                                object v = reader.GetValue(column);
                                if (v != null && value != null && (v.Equals(value) || (v.GetType() == typeof(string) && v.ToString().Trim().Equals(value.ToString().Trim()))))
                                {
                                    object[] row = new object[reader.FieldCount];
                                    for (int i = 0; i < row.Length; i++)
                                    {
                                        row[i] = reader.GetValue(i);
                                    }
                                    return row;
                                }
                            }

                        return new object[] { null };
                    }
                }
                catch (ThreadAbortException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
                }
                return null;
            }
        }

        public int IDMax(string tableName, int column)
        {
            return IDMax(tableName, column, DEF_ISOLATION_MODE);
        }
        public int IDMax(string tableName, int column, bool isolated)
        {
            int curr = 1;
            using (FbConnection Conn = new FbConnection(ConnectionString))
            {
                try
                {
                    Conn.Open();
                    string Sql = "SELECT * FROM " + tableName + " ORDER BY " + (column + 1) + " DESC";
                    using (FbCommand query = new FbCommand(Sql, Conn))
                    using (FbDataReader reader = query.ExecuteReader())
                    {

                        if (reader.HasRows)
                        {
                            reader.Read();
                            curr = Convert.ToInt32(reader.GetValue(column));
                        }
                        return curr;
                    }
                }
                catch (ThreadAbortException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
                    return -1;
                    //return e.Message.Contains("requires an open and available Connection") ? -1 : 0;
                }
            }
        }

        public object Last(string tableName, int column)
        {
            return Last(tableName, column, DEF_ISOLATION_MODE);
        }
        public object Last(string tableName, int column, bool isolated)
        {
            object curr = 1;
            using (FbConnection Conn = new FbConnection(ConnectionString))
            {
                try
                {
                    Conn.Open();
                    string Sql = "SELECT * FROM " + tableName + " ORDER BY 1 ASC";
                    using (FbCommand query = new FbCommand(Sql, Conn))
                    using (FbDataReader reader = query.ExecuteReader())
                    {
                        if (reader.HasRows)
                            while (reader.Read())
                            {
                                curr = reader.GetValue(column);
                            }
                        return curr;
                    }
                }
                catch (ThreadAbortException e)
                {
                    if (Conn != null)
                    {
                        Conn.Close();

                    }
                    throw e;
                }
                catch (Exception e)
                {
                    Console.WriteLine("DB ERROR: " + e.Message + "\r\n" + e.StackTrace);
                    return e.Message.Contains("requires an open and available Connection") ? (object)-1 : null;
                }
            }
        }

        /// <summary>
        /// Removes a table from cache.
        /// </summary>
        /// <param name="tableName">The name of the table to unload</param>
        /// <returns></returns>
        public bool UnloadTable(string tableName)
        {
            if (ExistsTable(tableName))
            {
                bool rem = TableCache.Remove(tableName);
                //if(rem && IndexCache.ContainsKey(tableName)){
                //IndexCache.Remove(tableName);
                //}
                GC.Collect();
                GC.WaitForPendingFinalizers();
                return rem;
            }
            return false;
        }

        public void UnloadAll()
        {
            TableCache.Clear();
            //IndexCache.Clear();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public List<string> GetAllColumnNames(string tableName)
        {
            string Sql = "select rf.rdb$field_name as column_name "
                + "from rdb$fields f join rdb$relation_fields rf "
                + "on rf.rdb$field_source = f.rdb$field_name "
                + "where rf.rdb$relation_name = '" + tableName + "';";
            DataTable t = GetTableUsing(Sql);
            List<string> cols = new List<string>();
            if (t != null)
            {
                foreach (DataRow r in t.Rows)
                {
                    cols.Add(r[0].ToString().Trim());
                }
            }
            return cols;
        }

        public List<string> GetAllTables()
        {
            string Sql = "select rdb$relation_name from rdb$relations "
                            + "where rdb$view_blr is null "
                            + "and (rdb$system_flag is null or rdb$system_flag=0) ORDER BY 1 ASC;";
            DataTable t = GetTableUsing(Sql);
            List<string> tables = new List<string>();
            if (t != null)
            {
                foreach (DataRow r in t.Rows)
                {
                    tables.Add(r[0].ToString().Trim());
                }
            }
            return tables;
        }

    }

    public class NameValuePair
    {
        public NameValuePair(object name, object value)
        {
            this.name = name;
            this.value = value;
        }

        public object name { get; private set; }
        public object value { get; private set; }
    }

    public static class ExtensionMethods
    {
        public static void PrintStackTrace(this Exception e)
        {
            Console.WriteLine(e.Message + "\r\n" + e.StackTrace);
        }

    }

}
