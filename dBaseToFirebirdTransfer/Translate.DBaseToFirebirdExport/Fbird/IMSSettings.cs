﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Linq;

namespace Fbird
{
    public class IMSSettings
    {
        private XDocument _xml;

        public IMSSettings()
        {
            _xml = null;
            Set("localhost",3050,"SYSDBA","masterkey","---",@"IMS");
        }

        public string Location { get; private set; }
        public string Server { get; private set; }
        public int Port { get; private set; }
        public string User { get; private set; }
        public string Pass { get; private set; }
        public string Alias { get; private set; }

        public void Set(string Server, int Port, string User, string Pass, string Alias, string Location)
        {
            this.Server = Server;
            this.Port = Port;
            this.User = User;
            this.Pass = Pass;
            this.Alias = Alias;
            this.Location = Location;
        }

        public bool LoadSettingsFrom(string fileLocation)
        {
            try
            {
                FileStream fs = new FileStream(fileLocation, FileMode.Open);
                StreamReader reader = new StreamReader(fs);
                string text = reader.ReadToEnd();
                _xml = XDocument.Parse(text);
                Explode();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\r\n" + e.StackTrace);
                return false;
            }
        }

        private void Explode()
        {
            var settings = _xml.Descendants("SETTINGS").First();
            Server = settings.Descendants("SERVER").First().Value;
            Port = Convert.ToInt32(settings.Descendants("PORT").First().Value);
            User = settings.Descendants("USER").First().Value;
            Pass = settings.Descendants("PASS").First().Value;
            Alias = settings.Descendants("ALIAS").First().Value;
            if (Alias == null || Alias.Length == 0) Alias = "---";
            Location = settings.Descendants("LOCATION").First().Value;
        }

    }
}
