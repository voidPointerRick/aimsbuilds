﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Translate.DBaseToFirebirdExport
{
    public delegate void ProgressObserver(int v, int max);

    public static class TransData
    {
        public static int WipeTable(Fbird.IMSSettings fbirdDestSettings, params string[] tableNames)
        {
            Fbird.DataUtils fbird = Fbird.DataUtils.Instance;
            Fbird.UpdateUtils fupdate = Fbird.UpdateUtils.Instance;

            List<string> param = new List<string>();
            string server = fbirdDestSettings.Server;
            if (server == null)
            {
                string dbLocation = fbirdDestSettings.Location;
                if (!fbird.OpenConnection(dbLocation)) throw new ArgumentException("The Selected Firebird Database Could Not Be Opened.");
            }
            else
            {
                string port = fbirdDestSettings.Port.ToString();
                string dbuser = fbirdDestSettings.User;
                string dbpass = fbirdDestSettings.Pass;
                string db = fbirdDestSettings.Alias == null || fbirdDestSettings.Alias.Equals("---") ? fbirdDestSettings.Location : fbirdDestSettings.Alias;
                bool alias = fbirdDestSettings.Alias == null || fbirdDestSettings.Alias.Equals("---");
                param.AddRange(new string[] { dbuser, dbpass, server, port.ToString() });
                if (alias) param.Add(db);
                if (!fbird.OpenConnection(alias ? null : db, param.ToArray())) throw new ArgumentException("The Selected Firebird Database Could Not Be Opened.");
            }

            List<string> sqls = new List<string>();
            foreach (string tableName in tableNames)
            {
                sqls.Add("DELETE FROM " + tableName + ";");
            }
            int[] upd = fupdate.RunUpdates(true, sqls.ToArray());
            return upd.Sum();
        }

        public static int Transfer(string dbaseSrcLocation, Fbird.IMSSettings fbirdDestSettings, string tableName, params ProgressObserver[] observers)
        {
            Dbase.DataUtils dbase = Dbase.DataUtils.Instance;
            Fbird.DataUtils fbird = Fbird.DataUtils.Instance;
            Fbird.UpdateUtils fupdate = Fbird.UpdateUtils.Instance;

            //Open DBF Database with MSJet Driver
            if (!dbase.OpenConnection(dbaseSrcLocation)) throw new ArgumentException("The Selected DBF Database Could Not Be Opened.");

            //Open Firebird Database with Firebird .NET Database Provier
            List<string> param = new List<string>();
            string server = fbirdDestSettings.Server;
            if (server == null)
            {
                string dbLocation = fbirdDestSettings.Location;
                if (!fbird.OpenConnection(dbLocation)) throw new ArgumentException("The Selected Firebird Database Could Not Be Opened.");
            }
            else
            {
                string port = fbirdDestSettings.Port.ToString();
                string dbuser = fbirdDestSettings.User;
                string dbpass = fbirdDestSettings.Pass;
                string db = fbirdDestSettings.Alias == null || fbirdDestSettings.Alias.Equals("---") ? fbirdDestSettings.Location : fbirdDestSettings.Alias;
                bool alias = fbirdDestSettings.Alias == null || fbirdDestSettings.Alias.Equals("---");
                param.AddRange(new string[] { dbuser, dbpass, server, port.ToString() });
                if (alias) param.Add(db);
                if (!fbird.OpenConnection(alias ? null : db, param.ToArray())) throw new ArgumentException("The Selected Firebird Database Could Not Be Opened.");
            }

            //Both Databases are opened. Start the transfer of data. 
            //(Re-format boolean data type for char(1) firebird from true/false to 'T'/'F' and trim fixed length strings to varchar strings)

            WConsole.Write("Loading " + tableName + " Table From The dBase Database...");

            try
            {
                DataTable src = dbase.GetTableUsing("select * from " + tableName + ";", true);

                List<string> sqls = new List<string>();
                string beginSql = "INSERT INTO " + tableName + " ";
                string sql = beginSql;
                int count = src.Rows.Count;
                List<int> result = new List<int>();

                Queue<Action> Workers = new Queue<Action>();
                Thread RunWorker = new Thread(() =>
                {
                    while (true)
                    {
                        if (Workers.Count > 0)
                        {
                            Action Run = Workers.Dequeue();
                            Run();
                        }
                        try
                        {
                            Thread.Sleep(50);
                        }
                        catch (ThreadInterruptedException e)
                        {
                            break;
                        }
                    }
                //Execute remaining workers and then exit.
                while (Workers.Count > 0)
                    {
                        Action T = Workers.Dequeue();
                        T();
                        Thread.Sleep(50);
                    }
                });

                RunWorker.Start();

                if (count > 0)
                {
                    int cutoff = count < 100 ? 1 : 100;
                    WConsole.Write("Preparing " + count + " Row" + (count == 1 ? "" : "s") + " For Transfer...");
                    for (int ro = 0; ro < count; ro++)
                    {
                        UpdateProgress(observers, ro + 1, count);
                        DataRow r = src.Rows[ro];
                        string rowSql = "SELECT ";
                        List<string> subsValues = new List<string>();
                        int col_count = src.Columns.Count;
                        if (tableName.ToLower().Equals("company_information"))
                        {
                            col_count++;
                        }
                        for (int i = 0; i < col_count; i++)
                        {
                            rowSql += "{" + i + "}";
                            if (i == col_count - 1)
                            {
                                rowSql += " FROM RDB$DATABASE";
                            }
                            else rowSql += ",";
                            if (i >= src.Columns.Count)
                            {
                                subsValues.Add(reformat(typeof(DBNull), DBNull.Value));
                            }
                            else
                            {
                                Type type = src.Columns[i].DataType;
                                object value = r[i];
                                subsValues.Add(reformat(type, value));
                            }
                        }
                        sql += string.Format(rowSql, subsValues.ToArray());
                        if (ro % cutoff == 0)
                        {
                            sql += ";";
                            string enqueSql = sql;
                            //sqls.Add(sql);
                            Workers.Enqueue(delegate ()
                            {
                                int res = fupdate.RunUpdate(enqueSql);
                                if (res == -1)
                                {
                                    ErrorRec.AddError(new ErrorRec()
                                    {
                                        errorTable = tableName,
                                        errorDesc = "TransferData Failed to insert SQL chunk into the database.\r\nCheck The Console For The Error Description.\r\nError Source SQL Code = "
                                                    + enqueSql
                                    });
                                }
                                result.Add(res);
                            });
                            sql = beginSql;
                            if (cutoff != 1) WConsole.Blue.Write(tableName + ": " + ro + " / " + count + " Rows Prepared...");
                        }
                        else
                        {
                            if (ro == count - 1)
                            {
                                sql += ";";
                                string enqueSql = sql;
                                WConsole.Blue.WriteLine(tableName + ": " + ro + " / " + count + " Rows Prepared");
                                //sqls.Add(sql);
                                Workers.Enqueue(delegate ()
                                {
                                    int res = fupdate.RunUpdate(enqueSql);
                                    if (res == -1)
                                    {
                                        ErrorRec.AddError(new ErrorRec()
                                        {
                                            errorTable = tableName,
                                            errorDesc = "TransferData Failed to insert SQL chunk into the database.\r\nCheck The Console For The Error Description.\r\nError Source SQL Code = "
                                                        + enqueSql
                                        });
                                    }
                                    result.Add(res);
                                });
                            }
                            else sql += " UNION ALL \r\n";
                        }
                    }
                    WConsole.Write("Waiting For Firebird Data Insert To Complete...");

                    Thread.Sleep(1000);
                    RunWorker.Interrupt();
                    RunWorker.Join();
                    fupdate.CloseSession();
                }
                else
                {
                    WConsole.WriteLine("No Data Found In " + tableName + ". Nothing To Transfer.");
                    result.Add(0);
                }

                fbird.CloseConnection();
                dbase.CloseConnection();

                return result.Sum();
            }catch(Exception e)
            {
                Console.Error.WriteLine(tableName+": Table Does Not Exist In The Database. Skippping...");
                return 0;
            }

        }

        private static void UpdateProgress(ProgressObserver[] o, int v, int m)
        {
            foreach (ProgressObserver progress in o)
            {
                progress(v, m);
            }
        }

        private static string reformat(Type dataType, object tbValue)
        {
            if (tbValue == DBNull.Value) return "NULL";
            if (dataType == typeof(int)) return Convert.ToInt32(tbValue).ToString();
            if (dataType == typeof(char)) return "'" + Convert.ToChar(tbValue) + "'";
            if (dataType == typeof(bool)) return Convert.ToBoolean(tbValue).FromBoolean();
            if (dataType == typeof(double)) return Convert.ToDouble(tbValue).ToString();
            if (dataType == typeof(float)) return Convert.ToSingle(tbValue).ToString();
            if (dataType == typeof(DateTime)) return "'" + escape(Convert.ToDateTime(tbValue).toDatabaseDate()) + "'";
            return "'" + escape(tbValue.ToString().Trim()) + "'";
        }

        public static string toDatabaseDate(this DateTime dt)
        {
            return dt.Month + "/" + dt.Day + "/" + dt.Year;
        }

        public static string FromBoolean(this bool b)
        {
            return b ? "'T'" : "'F'";
        }

        public static string escape(string str)
        {
            str = str.Replace("'", "\'\'");
            str = str.Replace("\"\"", "\"");
            return str;
        }

    }
}
