﻿namespace Translate.DBaseToFirebirdExport
{
    partial class TransferWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Input_DBF = new System.Windows.Forms.TextBox();
            this.Button_DBF = new System.Windows.Forms.Button();
            this.Button_Firebird = new System.Windows.Forms.Button();
            this.Input_Firebird = new System.Windows.Forms.TextBox();
            this.Button_LoadSetting = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Button_CreateSetting = new System.Windows.Forms.Button();
            this.Button_Begin_Trans = new System.Windows.Forms.Button();
            this.Status_Bar = new System.Windows.Forms.StatusStrip();
            this.Status_Progress = new System.Windows.Forms.ToolStripProgressBar();
            this.Status_Text = new System.Windows.Forms.ToolStripStatusLabel();
            this.Output_Console = new System.Windows.Forms.RichTextBox();
            this.Check_WipeData = new System.Windows.Forms.CheckBox();
            this.Check_Pause = new System.Windows.Forms.CheckBox();
            this.Button_Errors = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Status_Bar.SuspendLayout();
            this.SuspendLayout();
            // 
            // Input_DBF
            // 
            this.Input_DBF.BackColor = System.Drawing.Color.White;
            this.Input_DBF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Input_DBF.Location = new System.Drawing.Point(8, 16);
            this.Input_DBF.Name = "Input_DBF";
            this.Input_DBF.ReadOnly = true;
            this.Input_DBF.Size = new System.Drawing.Size(456, 21);
            this.Input_DBF.TabIndex = 0;
            // 
            // Button_DBF
            // 
            this.Button_DBF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_DBF.Location = new System.Drawing.Point(470, 16);
            this.Button_DBF.Name = "Button_DBF";
            this.Button_DBF.Size = new System.Drawing.Size(220, 21);
            this.Button_DBF.TabIndex = 1;
            this.Button_DBF.Text = "Select DBF Database Location";
            this.Button_DBF.UseVisualStyleBackColor = true;
            this.Button_DBF.Click += new System.EventHandler(this.Button_DBF_Click);
            // 
            // Button_Firebird
            // 
            this.Button_Firebird.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Firebird.Location = new System.Drawing.Point(470, 11);
            this.Button_Firebird.Name = "Button_Firebird";
            this.Button_Firebird.Size = new System.Drawing.Size(220, 21);
            this.Button_Firebird.TabIndex = 3;
            this.Button_Firebird.Text = "Select Firebird Database File";
            this.Button_Firebird.UseVisualStyleBackColor = true;
            this.Button_Firebird.Click += new System.EventHandler(this.Button_Firebird_Click);
            // 
            // Input_Firebird
            // 
            this.Input_Firebird.BackColor = System.Drawing.Color.White;
            this.Input_Firebird.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Input_Firebird.Location = new System.Drawing.Point(8, 38);
            this.Input_Firebird.Name = "Input_Firebird";
            this.Input_Firebird.ReadOnly = true;
            this.Input_Firebird.Size = new System.Drawing.Size(456, 21);
            this.Input_Firebird.TabIndex = 2;
            // 
            // Button_LoadSetting
            // 
            this.Button_LoadSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_LoadSetting.Location = new System.Drawing.Point(470, 67);
            this.Button_LoadSetting.Name = "Button_LoadSetting";
            this.Button_LoadSetting.Size = new System.Drawing.Size(220, 23);
            this.Button_LoadSetting.TabIndex = 4;
            this.Button_LoadSetting.Text = "Load From IMSSettings XML";
            this.Button_LoadSetting.UseVisualStyleBackColor = true;
            this.Button_LoadSetting.Click += new System.EventHandler(this.Button_LoadSetting_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Button_DBF);
            this.groupBox1.Controls.Add(this.Input_DBF);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(702, 47);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "IMS DBase Database Location";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Button_CreateSetting);
            this.groupBox2.Controls.Add(this.Button_LoadSetting);
            this.groupBox2.Controls.Add(this.Button_Firebird);
            this.groupBox2.Controls.Add(this.Input_Firebird);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(702, 98);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Firebird Database Settings";
            // 
            // Button_CreateSetting
            // 
            this.Button_CreateSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_CreateSetting.Location = new System.Drawing.Point(470, 38);
            this.Button_CreateSetting.Name = "Button_CreateSetting";
            this.Button_CreateSetting.Size = new System.Drawing.Size(220, 23);
            this.Button_CreateSetting.TabIndex = 5;
            this.Button_CreateSetting.Text = "Change Firebird Access Settings";
            this.Button_CreateSetting.UseVisualStyleBackColor = true;
            this.Button_CreateSetting.Click += new System.EventHandler(this.Button_CreateSetting_Click);
            // 
            // Button_Begin_Trans
            // 
            this.Button_Begin_Trans.Location = new System.Drawing.Point(247, 555);
            this.Button_Begin_Trans.Name = "Button_Begin_Trans";
            this.Button_Begin_Trans.Size = new System.Drawing.Size(220, 45);
            this.Button_Begin_Trans.TabIndex = 7;
            this.Button_Begin_Trans.Text = "Begin Data Transfer";
            this.Button_Begin_Trans.UseVisualStyleBackColor = true;
            this.Button_Begin_Trans.Click += new System.EventHandler(this.Button_Begin_Trans_Click);
            // 
            // Status_Bar
            // 
            this.Status_Bar.AutoSize = false;
            this.Status_Bar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Status_Progress,
            this.Status_Text});
            this.Status_Bar.Location = new System.Drawing.Point(0, 604);
            this.Status_Bar.Name = "Status_Bar";
            this.Status_Bar.Size = new System.Drawing.Size(728, 25);
            this.Status_Bar.SizingGrip = false;
            this.Status_Bar.TabIndex = 8;
            // 
            // Status_Progress
            // 
            this.Status_Progress.Name = "Status_Progress";
            this.Status_Progress.Size = new System.Drawing.Size(450, 19);
            this.Status_Progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // Status_Text
            // 
            this.Status_Text.Name = "Status_Text";
            this.Status_Text.Size = new System.Drawing.Size(39, 20);
            this.Status_Text.Text = "Ready";
            // 
            // Output_Console
            // 
            this.Output_Console.BackColor = System.Drawing.Color.Black;
            this.Output_Console.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Output_Console.ForeColor = System.Drawing.Color.White;
            this.Output_Console.Location = new System.Drawing.Point(12, 224);
            this.Output_Console.Name = "Output_Console";
            this.Output_Console.ReadOnly = true;
            this.Output_Console.Size = new System.Drawing.Size(704, 326);
            this.Output_Console.TabIndex = 9;
            this.Output_Console.Text = "";
            this.Output_Console.TextChanged += new System.EventHandler(this.Output_Console_TextChanged);
            // 
            // Check_WipeData
            // 
            this.Check_WipeData.AutoSize = true;
            this.Check_WipeData.Location = new System.Drawing.Point(247, 188);
            this.Check_WipeData.Name = "Check_WipeData";
            this.Check_WipeData.Size = new System.Drawing.Size(220, 17);
            this.Check_WipeData.TabIndex = 10;
            this.Check_WipeData.Text = "Wipe Firebird Table Data Before Transfer";
            this.Check_WipeData.UseVisualStyleBackColor = true;
            // 
            // Check_Pause
            // 
            this.Check_Pause.Appearance = System.Windows.Forms.Appearance.Button;
            this.Check_Pause.Location = new System.Drawing.Point(574, 173);
            this.Check_Pause.Name = "Check_Pause";
            this.Check_Pause.Size = new System.Drawing.Size(128, 45);
            this.Check_Pause.TabIndex = 11;
            this.Check_Pause.Text = "Pause Before \r\nNext Table Access";
            this.Check_Pause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Check_Pause.UseVisualStyleBackColor = true;
            // 
            // Button_Errors
            // 
            this.Button_Errors.Location = new System.Drawing.Point(20, 173);
            this.Button_Errors.Name = "Button_Errors";
            this.Button_Errors.Size = new System.Drawing.Size(128, 45);
            this.Button_Errors.TabIndex = 12;
            this.Button_Errors.Text = "Show Errors";
            this.Button_Errors.UseVisualStyleBackColor = true;
            this.Button_Errors.Visible = false;
            this.Button_Errors.Click += new System.EventHandler(this.Button_Errors_Click);
            // 
            // TransferWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 629);
            this.Controls.Add(this.Button_Errors);
            this.Controls.Add(this.Check_Pause);
            this.Controls.Add(this.Check_WipeData);
            this.Controls.Add(this.Output_Console);
            this.Controls.Add(this.Status_Bar);
            this.Controls.Add(this.Button_Begin_Trans);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "TransferWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transfer IMS Data To Firebird";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Status_Bar.ResumeLayout(false);
            this.Status_Bar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Input_DBF;
        private System.Windows.Forms.Button Button_DBF;
        private System.Windows.Forms.Button Button_Firebird;
        private System.Windows.Forms.TextBox Input_Firebird;
        private System.Windows.Forms.Button Button_LoadSetting;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Button_CreateSetting;
        private System.Windows.Forms.Button Button_Begin_Trans;
        private System.Windows.Forms.StatusStrip Status_Bar;
        private System.Windows.Forms.ToolStripProgressBar Status_Progress;
        private System.Windows.Forms.ToolStripStatusLabel Status_Text;
        private System.Windows.Forms.RichTextBox Output_Console;
        private System.Windows.Forms.CheckBox Check_WipeData;
        private System.Windows.Forms.CheckBox Check_Pause;
        private System.Windows.Forms.Button Button_Errors;
    }
}

