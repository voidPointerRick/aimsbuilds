﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Translate.DBaseToFirebirdExport
{

    public partial class TransferWindow : Form
    {

        private string dbaseLocation;
        private Fbird.IMSSettings firebirdLocation;

        public TransferWindow()
        {
            InitializeComponent();
            dbaseLocation = @"C:\ims test data\";
            firebirdLocation = new Fbird.IMSSettings();
            ErrorRec.OnUpdate = delegate(object o, EventArgs ea)
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(delegate
                    {
                        Button_Errors.Visible = ErrorRec.GetAll().Count > 0;
                    }));
                }
                else
                {
                    Button_Errors.Visible = ErrorRec.GetAll().Count > 0;
                }
            };

            Icon = Properties.Resources.database_transfer_icon;

            TextWriter w = new TextBoxWriter(Output_Console, Color.White);
            Console.SetOut(w);
            TextWriter e = new TextBoxWriter(Output_Console, Color.Red);
            Console.SetError(e);

            TextWriter y = new TextBoxWriter(Output_Console, Color.Yellow);
            TextWriter b = new TextBoxWriter(Output_Console, Color.Blue);
            TextWriter g = new TextBoxWriter(Output_Console, Color.Green);
            WConsole.SetOut(y);
            WConsole.SetBlueOut(b);
            WConsole.SetGreenOut(g);

            WConsole.Blue.WriteLine("Please disconnect all other applications from the selected database before starting the transfer.");

            DataOutputRefresh();
        }

        private void DataOutputRefresh()
        {
            Input_DBF.Text = dbaseLocation;
            Input_Firebird.Text
                = (firebirdLocation.Alias == null || firebirdLocation.Alias.Equals("---"))
                ? firebirdLocation.Location : firebirdLocation.Alias;
        }

        bool active = false;

        private void Button_Begin_Trans_Click(object sender, EventArgs e)
        {
            if (active) return;
            Output_Console.Clear();
            EnableControls(false);
            ThreadPool.QueueUserWorkItem((object o) =>
            {
                active = true;
                string[] table_order = new string[]{
                    "application_global_config",
                    "auth_permissions","auth_users","auth_info",
                    "supplier_information","user_information",
                    "salesman","categories","location_code",
                    "measuring_unit","tax_code",
                    "inventory_information",
                    "change_log","reason_log",
                    "branches","company_information",
                    "customer_type","customer_information",
                    "credit_payment",
                    "holddetails","holdhead",
                    "quote_holddetails","quote_holdhead",
                    "auto_holddetails","auto_holdhead",
                    "delivery_details","delivery_reference",
                    "invoice_detailsu","invoice_referenceu",
                    "invoice_details","invoice_reference",
                    "po_header","po_details",
                    "quote_reference","quote_details"
                };

                ErrorRec.Clear();

                try
                {
                    if (Check_WipeData.Checked)
                    {
                        WConsole.WriteLine("Deleting Data From All Destination Tables...");
                        int del = TransData.WipeTable(firebirdLocation, table_order.Reverse().ToArray());
                        Console.Error.WriteLine("Total Rows Deleted: " + del);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error.\r\n" + ex.Message);
                    Console.Error.WriteLine(ex.Message);
                    Console.Error.WriteLine("------------------------------------------------------");
                    Console.Error.WriteLine("Transfer Failed.");
                    EnableControls(true);
                    active = false;
                    return;
                }
                try
                {
                    foreach (string table in table_order)
                    {
                        WConsole.WriteLine("Starting Transfer of " + table + " ...");
                        while (Check_Pause.Checked)
                        {
                            Thread.Sleep(200);
                        }
                        int fmax = -1;
                        int res = TransData.Transfer(dbaseLocation, firebirdLocation, table, delegate(int v, int max)
                        {
                            if (fmax == -1) fmax = max;
                            InvokeUpdateProgress(table, v, max);
                        });
                        if (res == -1)
                        {
                            Console.Error.WriteLine("An error occurred while transfering " + table);
                        }
                        else if (res > 0)
                        {
                            WConsole.WriteLine("Transfer of " + table + " is Complete. " + res + " of " + fmax + " Rows Added");
                        }
                        InvokeUpdateProgress(table, 0, 100);
                        Thread.Sleep(1000);
                        Console.WriteLine("---------");
                    }
                    Console.WriteLine(">>>>----------------------------------------------<<<<");
                    if (ErrorRec.GetAll().Count > 0)
                    {
                        WConsole.Blue.WriteLine("Transfer Complete With "+ErrorRec.GetAll().Count+" Errors.");
                        WConsole.Blue.WriteLine("Click 'Show Errors' to view error details");
                    }
                    else
                    {
                        WConsole.Green.WriteLine("Transfer Complete.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error.\r\n" + ex.Message);
                    Console.Error.WriteLine(ex.Message);
                    Console.Error.WriteLine(ex.StackTrace);
                    Console.Error.WriteLine("------------------------------------------------------");
                    Console.Error.WriteLine("Transfer Failed.");
                }
                EnableControls(true, Button_Begin_Trans);
                active = false;
            });

        }

        private void EnableControls(bool enable, params Control[] Ignore)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<bool, Control[]>(EnableControls), enable, Ignore);
                return;
            }
            foreach (Control c in new Control[]{Input_DBF, Button_DBF, Input_Firebird, Button_Firebird, 
                Button_CreateSetting, Button_LoadSetting, Button_Begin_Trans, Check_WipeData})
            {
                if (Ignore.Contains(c)) continue;
                c.Enabled = false;
            }
        }

        private void InvokeUpdateProgress(string tbName, int v, int max)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string, int, int>(InvokeUpdateProgress), tbName, v, max);
                return;
            }
            Status_Progress.Maximum = max;
            Status_Progress.Value = v;
            Status_Text.Text = v == 0 ? "Ready" : v == max
                ? "Inserting into " + tbName + "..." : (v + " / " + max + " Prepared from " + tbName);
            Status_Progress.Style = (v == max) ? ProgressBarStyle.Marquee : ProgressBarStyle.Continuous;
            Status_Progress.Invalidate();
        }

        private void Button_DBF_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                string path = dlg.SelectedPath;
                dbaseLocation = path;
                DataOutputRefresh();
            }
        }

        private void Button_Firebird_Click(object sender, EventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog();
            fbd.Filter = "Firebird Database (*.fdb;*.gdb) | *.fdb;*.gdb";
            fbd.Multiselect = false;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                firebirdLocation.Set(null, 0, null, null, null, fbd.FileName);
                DataOutputRefresh();
            }
        }

        private void Button_CreateSetting_Click(object sender, EventArgs e)
        {
            Fbird.DBSetup s = new Fbird.DBSetup(firebirdLocation);
            if (s.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DataOutputRefresh();
            }
        }

        private void Button_LoadSetting_Click(object sender, EventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog();
            fbd.Filter = "XML Settings (*.xml) | *.xml";
            fbd.Multiselect = false;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                firebirdLocation.LoadSettingsFrom(fbd.FileName);
                DataOutputRefresh();
            }
        }

        private void Output_Console_TextChanged(object sender, EventArgs e)
        {
            if (Output_Console.Text.Length > 2147481647)//MaxLen - 2000 chars
            {
                int maxLine = Output_Console.Lines.Length;
                Output_Console.Select(0, Output_Console.GetFirstCharIndexFromLine(20 >= maxLine ? maxLine - 1 : 20)); // select the first 20 line
                Output_Console.SelectedText = "";//And delete them
            }
        }

        private void Button_Errors_Click(object sender, EventArgs e)
        {
            Form f = new Form();

            TextBox b = new TextBox();
            b.ReadOnly = true;
            b.Multiline = true;
            b.WordWrap = true;
            b.ScrollBars = ScrollBars.Both;
            b.Text = string.Join(
                "\r\n---------------------------------------------------------------------------\r\n", 
                (from x in ErrorRec.GetAll()
                 orderby x.errorTable ascending
                 select "Table: " + x.errorTable + "\r\nDescription: " + x.errorDesc));
            b.Select(0, 0);
            b.Dock = DockStyle.Fill;
            b.Font = new System.Drawing.Font("MS Sans Serif", 13f);

            f.Controls.Add(b);
            f.Text = "Transfer Errors";
            f.StartPosition = FormStartPosition.CenterParent;
            f.WindowState = FormWindowState.Maximized;
            f.ShowDialog(this);
        }
    }

    public static class WConsole
    {
        public static TextWriter Blue { get; private set; }

        public static TextWriter Green { get; private set; }

        public static TextWriter Out { get; private set; }

        public static void SetOut(TextWriter Out)
        {
            WConsole.Out = Out;
        }

        public static void SetBlueOut(TextWriter Blue)
        {
            WConsole.Blue = Blue;
        }

        public static void SetGreenOut(TextWriter Green)
        {
            WConsole.Green = Green;
        }

        public static void Write(string o)
        {
            if (Out == null) Console.Out.Write(o);
            else Out.Write(o);
        }

        public static void WriteLine(string o)
        {
            if (Out == null) Console.Out.Write(o + "\r\n");
            else Out.WriteLine(o);
        }

    }

    public static class Ext
    {

        public static void AppendText(this RichTextBox box, string text, Color color, bool AddNewLine = false)
        {
            if (AddNewLine)
            {
                text += Environment.NewLine;
            }

            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

    }

    public class TextBoxWriter : TextWriter
    {
        private RichTextBox Output_Console;
        private Color textColor;

        public TextBoxWriter(RichTextBox Output_Console, Color textColor)
        {
            this.Output_Console = Output_Console;
            this.textColor = textColor;
        }

        public override void WriteLine(string value)
        {
            if (!value.EndsWith("\r\n"))
                value += "\r\n";
            Write(value);
        }

        public override void Write(string value)
        {
            try
            {
                if (Output_Console.InvokeRequired)
                {
                    Output_Console.Invoke(new Action<string>(WriteLine), value);
                    return;
                }
                Output_Console.AppendText(value, textColor);
                Output_Console.Select(Output_Console.Text.Length, 0);
                Output_Console.ScrollToCaret();
            }
            catch (Exception) { }
        }

        public override Encoding Encoding
        {
            get
            {
                return Encoding.UTF8;
            }
        }

    }

    public class ErrorRec
    {
        private static EventHandler _evt;
        public static EventHandler OnUpdate
        {
            get
            {
                if (_evt == null) return DefaultUpd;
                return _evt;
            }
            set
            {
                _evt = value;
            }
        }
        private static void DefaultUpd(object sender, EventArgs e)
        {
            
        }

        private static readonly List<ErrorRec> Errors = new List<ErrorRec>();

        public static void AddError(ErrorRec rec)
        {
            Errors.Add(rec);
            OnUpdate(rec, new EventArgs());
        }
        public static List<ErrorRec> GetAll()
        {
            return Errors;
        }
        public static void Clear()
        {
            Errors.Clear();
            OnUpdate(new object(), new EventArgs());
        }

        public string errorTable;
        public string errorDesc;

    }
}
